import Vue from "nativescript-vue";

import App from "./components/App";
import LoginPage from "./components/Login";
Vue.config.silent = false;
new Vue({
    render: h => h('frame', { props: { id: 'rootFrame' } }, [h(LoginPage)])
}).$start();
